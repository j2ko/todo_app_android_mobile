package com.example.todoappandroidmobile.database

/**
* Defines a database and specifies data tables that will be used.
* Version is incremented as new tables/columns are added/removed/changed.
*/
@androidx.room.Database(entities = [com.example.todoappandroidmobile.database.model.TodoItem::class], version = 1)
abstract class TodoAppDatabase : androidx.room.RoomDatabase() {
    abstract fun todoItemDao(): TodoItemDao

    companion object {
        private var INSTANCE: TodoAppDatabase? = null


        fun getDatabase(context: android.content.Context): TodoAppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = androidx.room.Room.databaseBuilder(
                    context.applicationContext,
                    TodoAppDatabase::class.java,
                    "todoapp"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}