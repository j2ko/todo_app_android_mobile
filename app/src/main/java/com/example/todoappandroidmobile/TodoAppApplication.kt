package com.example.todoappandroidmobile

import android.app.Application
import com.example.todoappandroidmobile.database.TodoAppDatabase

class TodoAppApplication: Application() {
    val database: TodoAppDatabase by lazy { TodoAppDatabase.getDatabase(this) }
}