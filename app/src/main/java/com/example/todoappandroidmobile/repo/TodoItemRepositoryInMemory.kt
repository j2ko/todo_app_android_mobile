package com.example.todoappandroidmobile.repo

import com.example.todoappandroidmobile.database.model.TodoItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.onStart

/* write TodoItemRepositoryInMemory that should implement TodoItemRepository interface:
   - implement each method defined in TodoItemRepository interface
   - put actual implementation of each method instead of TODO comments
   - keep TodoItem list in internalItems field
   - use MutableSharedFlow to emit TodoItem list when it's changed
   - return MutableSharedFlow as Flow from items property
 */
class TodoItemRepositoryInMemory : TodoItemRepository {
    private val internalItems = mutableListOf<TodoItem>()
    private val itemsFlow = MutableSharedFlow<List<TodoItem>>(replay = 1)

    override val items: Flow<List<TodoItem>> = itemsFlow.onStart { emit (emptyList()) }

    override suspend fun add(item: TodoItem) {
        internalItems.add(item)
        itemsFlow.emit(internalItems)
    }

    override suspend fun delete(item: TodoItem) {
        internalItems.remove(item)
        itemsFlow.emit(internalItems)
    }

    //write compete method that will mark TodoItem as completed by replacing list element with new value
    //and emit updated list of TodoItem items
    override suspend fun complete(item: TodoItem) {
        val index = internalItems.indexOf(item)
        if (index != -1) {
            internalItems[index] = item.copy(isCompleted = true)
            itemsFlow.emit(internalItems)
        }
    }
}