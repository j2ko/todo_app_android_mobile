package com.example.todoappandroidmobile.repo

import com.example.todoappandroidmobile.database.TodoItemDao
import com.example.todoappandroidmobile.database.model.TodoItem

// implement TodoItemRepository interface that use TodoItemDao to get TodoItem items as Flow
// and to add, delete and complete TodoItem items
class TodoItemRepositoryDatabase(val dao: TodoItemDao) : TodoItemRepository {
    override val items = dao.getAll()

    override suspend fun add(item: TodoItem) {
        dao.add(item)
    }

    override suspend fun delete(item: TodoItem) {
        dao.delete(item)
    }

    override suspend fun complete(item: TodoItem) {
        dao.update(item.copy(isCompleted = true))
    }
}