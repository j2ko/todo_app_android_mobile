package com.example.todoappandroidmobile.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.todoappandroidmobile.database.model.TodoItem
import com.example.todoappandroidmobile.repo.TodoItemRepository
import com.example.todoappandroidmobile.ui.TodoAppViewModel
import com.example.todoappandroidmobile.ui.theme.TodoAppAndroidMobileTheme
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/* Create Composable view TodoAppScreen that will take as input viewModele and collect as state TooAppUiState objects.
 It should display following UI elements:
 - view should be scaled to fill the screen
 - Input text for todo item should fill entire width of the screen with 15dp horizontal padding
 - Input text value should be set only from uiState.text, all changed to input text should be propagated to viewModel using updateText method
 - Add button should be placed below intput and also fill entire width 15dp horizontal padding
 - List of todo items should be displayed as a table with 2 columns
 - First row should contain column names: "Todo item" and "Actions"
 - For each item in Todo items list there should be displayed row where:
     First column should contain todo item text
     Second column should contain "Complete" and "Delete" action buttons
 - add preview for this view
 - if item is completed it should be displayed with strikethrough text style
 - if item is completed button Complete should be hidden
 */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TodoAppScreen(viewModel: TodoAppViewModel = viewModel()) {
    val uiState = viewModel.uiState.collectAsState()
    Column(modifier = Modifier.fillMaxSize()) {
        TextField(
            value = uiState.value.text,
            onValueChange = viewModel::updateText,
            label = { Text("Todo item") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(15.dp)
        )
        Button(
            onClick = viewModel::addItem,
            modifier = Modifier
                .fillMaxWidth()
                .padding(15.dp)
        ) {
            Text("Add")
        }
        LazyColumn {
            item {
                Row {
                    Text("Todo item", modifier = Modifier.weight(1f))
                    Text("Actions")
                }
            }
            items(uiState.value.items) { item ->
                Row {
                    Text(
                        item.text,
                        modifier = Modifier
                            .weight(1f)
                            .padding(15.dp),
                        textDecoration = if (item.isCompleted) TextDecoration.LineThrough else TextDecoration.None
                    )
                    if (!item.isCompleted) {
                        Button(onClick = { viewModel.completeItem(item) }) {
                            Text("Complete")
                        }
                    }
                    Button(onClick = { viewModel.deleteItem(item) }) {
                        Text("Delete")
                    }
                }
            }
        }
    }
}

// create dummy TodoItemRepository implementation that will be used for preview
// it should contain 2 items: "Item 1", "Item 2". Item 1 should be completed
// do not use update method
// construct it using flow { emit(...) } builder
class DummyTodoItemRepository : TodoItemRepository {
    override val items: Flow<List<TodoItem>> = flow {
        emit(
            listOf(
                TodoItem("Item 1", true),
                TodoItem("Item 2", false)
            )
        )
    }

    override suspend fun add(item: TodoItem) = Unit

    override suspend fun delete(item: TodoItem) = Unit

    override suspend fun complete(item: TodoItem) = Unit
}

// implement preview for TodoAppScreen that will use ViewModel with DummyTodoItemRepository
@Preview(showBackground = true)
@Composable
fun TodoAppScreenPreview() {
    TodoAppAndroidMobileTheme {
        TodoAppScreen(TodoAppViewModel(DummyTodoItemRepository()))
    }
}