package com.example.todoappandroidmobile

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.example.todoappandroidmobile.database.TodoAppDatabase
import com.example.todoappandroidmobile.ui.screen.TodoAppScreen
import com.example.todoappandroidmobile.ui.TodoAppViewModel
import com.example.todoappandroidmobile.ui.theme.TodoAppAndroidMobileTheme

class MainActivity : ComponentActivity() {
    private val viewModel : TodoAppViewModel by viewModels { TodoAppViewModel.Factory((application as TodoAppApplication).database) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TodoAppAndroidMobileTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    TodoAppScreen(viewModel)
                }
            }
        }
    }
}


