FROM cimg/android:2023.11.1 as build

# Create app directory
WORKDIR /todo_app

# Clone the Android Todo App repository
RUN git clone https://gitlab.com/j2ko/todo_app_android_mobile.git .

# Specify the target variant (you may need to adjust this based on your project)
ARG VARIANT=debug

# Build the APK
RUN ./gradlew assemble${VARIANT}

FROM scratch as artifact

ARG VARIANT=debug

COPY --from=build /todo_app/app/build/outputs/apk/${VARIANT}/app-${VARIANT}.apk /app.apk